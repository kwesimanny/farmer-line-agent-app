package co.farmerline.farmerlineagent.Realm.repositories;

import android.support.annotation.NonNull;

import co.farmerline.farmerlineagent.Pojo.Product;
import io.realm.Realm;
import io.realm.RealmResults;

public class ProductRepository {

    private Realm mRealm;

    public ProductRepository(Realm realm) {
        mRealm = realm;
    }

    public void addProduct(final Product product) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                try {
                    mRealm.copyToRealmOrUpdate(product);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public RealmResults<Product> loadAll() {
        return mRealm.where(Product.class).findAll();
    }

    public long count() {
        return mRealm.where(Product.class).count();
    }
}
