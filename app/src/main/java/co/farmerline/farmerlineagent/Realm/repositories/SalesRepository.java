package co.farmerline.farmerlineagent.Realm.repositories;

import co.farmerline.farmerlineagent.Pojo.Sale;
import io.realm.Realm;
import io.realm.RealmResults;

public class SalesRepository {
    private final Realm mRealm;

    public SalesRepository(Realm realm) {
        this.mRealm = realm;
    }

    public RealmResults<Sale> loadAll() {
        return mRealm.where(Sale.class).findAll();
    }
}
