package co.farmerline.farmerlineagent.Realm;


import co.farmerline.farmerlineagent.Realm.repositories.AgentRepository;
import co.farmerline.farmerlineagent.Realm.repositories.ProductRepository;
import co.farmerline.farmerlineagent.Realm.repositories.SalesRepository;
import io.realm.Realm;

//This class uses Realm to aid in the offline storage of data.
//In the case of bad internet connectivity, entered sales, could be stored
//offline and resent to the server when better internet service returns

public class RealmManager {

    private static Realm mRealm;

    public static void open() {
        mRealm = Realm.getDefaultInstance();
    }

    public static void close() {
        if (mRealm != null) {
            mRealm.close();
        }
    }public static AgentRepository agentRepository() {
        checkForOpenRealm();
        return new AgentRepository(mRealm);
    }

    public static ProductRepository productRepository() {
        checkForOpenRealm();
        return new ProductRepository(mRealm);
    }

    public static SalesRepository salesRepository() {
        checkForOpenRealm();
        return new SalesRepository(mRealm);
    }

    private static void checkForOpenRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            throw new IllegalStateException("RealmManager: Realm is closed, call open() method first");
        }
    }

}
