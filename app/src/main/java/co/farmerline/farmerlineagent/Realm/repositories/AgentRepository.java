package co.farmerline.farmerlineagent.Realm.repositories;

import co.farmerline.farmerlineagent.Pojo.Agent;
import io.realm.Realm;

public class AgentRepository {
    private final Realm mRealm;

    public AgentRepository(Realm realm) {
        this.mRealm = realm;
    }

    public Agent getAgent() {
        return mRealm.where(Agent.class).findFirst();
    }
}
