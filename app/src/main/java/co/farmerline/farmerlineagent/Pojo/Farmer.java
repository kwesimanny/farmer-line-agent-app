package co.farmerline.farmerlineagent.Pojo;

import io.realm.annotations.PrimaryKey;

public class Farmer {

    @PrimaryKey
    private long saleRealmId;

    private String farmerName;
    private String farmerId;
    private String farmerPhone;

    public long getSaleRealmId() {
        return saleRealmId;
    }

    public void setSaleRealmId(long saleRealmId) {
        this.saleRealmId = saleRealmId;
    }

    public String getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(String farmerName) {
        this.farmerName = farmerName;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFarmerPhone() {
        return farmerPhone;
    }

    public void setFarmerPhone(String farmerPhone) {
        this.farmerPhone = farmerPhone;
    }
}
