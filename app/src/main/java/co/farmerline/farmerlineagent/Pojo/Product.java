package co.farmerline.farmerlineagent.Pojo;

import co.farmerline.farmerlineagent.Realm.RealmManager;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject {

    @PrimaryKey
    private long productRealmId;

    private String productName;
    private double productPrice;
    private int quantityAvailable;
    private int quantityLeft;

    public Product() {
        productRealmId = RealmManager.productRepository().count() + 1;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public int getQuantityLeft() {
        return quantityLeft;
    }

    public void setQuantityLeft(int quantityLeft) {
        this.quantityLeft = quantityLeft;
    }

    public int getQuantitySold() {
        return quantityAvailable - quantityLeft;
    }


    public double getTotalSoldPrice() {
        return getQuantitySold() * productPrice;
    }
}
