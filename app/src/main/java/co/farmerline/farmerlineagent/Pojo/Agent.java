package co.farmerline.farmerlineagent.Pojo;

import io.realm.RealmObject;

public class Agent extends RealmObject {
    private String agentId;
    private String lastLogin;
    private int farmersSoldTo;
    private int farmersQuota;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public int getFarmersSoldTo() {
        return farmersSoldTo;
    }

    public void setFarmersSoldTo(int farmersSoldTo) {
        this.farmersSoldTo = farmersSoldTo;
    }

    public int getFarmersQuota() {
        return farmersQuota;
    }

    public void setFarmersQuota(int farmersQuota) {
        this.farmersQuota = farmersQuota;
    }
}
