package co.farmerline.farmerlineagent.Pojo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Sale extends RealmObject {

    @PrimaryKey
    private long saleRealmId;

    private long productRealmId;
    private String farmerId;
    private String farmLocation;
    private String agentId;
    private String date;
    private int quantityBought;
    private double productPrice;


    public Sale() {
    }

    public long getProductRealmId() {
        return productRealmId;
    }

    public void setProductRealmId(long productRealmId) {
        this.productRealmId = productRealmId;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getFarmLocation() {
        return farmLocation;
    }

    public void setFarmLocation(String farmLocation) {
        this.farmLocation = farmLocation;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(int quantityBought) {
        this.quantityBought = quantityBought;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public double getCostOfSale() {
        return quantityBought * productPrice;
    }

}
