package co.farmerline.farmerlineagent.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.farmerline.farmerlineagent.Fragments.MapFragment;
import co.farmerline.farmerlineagent.Fragments.OverviewFragment;
import co.farmerline.farmerlineagent.Pojo.Product;
import co.farmerline.farmerlineagent.R;
import co.farmerline.farmerlineagent.Realm.RealmManager;

public class MainActivity extends AppCompatActivity {

    final FragmentManager fm = getSupportFragmentManager();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    OverviewFragment overviewFragment = new OverviewFragment();
    MapFragment mapFragment = new MapFragment();
    private Fragment curFragment = overviewFragment;


    private void dummyData(){
        Product product = new Product();
        product.setProductName("Confidor");
        product.setProductPrice(166);
        product.setQuantityAvailable(100);
        product.setQuantityLeft(30);

        RealmManager.productRepository().addProduct(product);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        RealmManager.open();
        dummyData();

        toolbar.setTitle("Agent Overview");
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        fm.beginTransaction().add(R.id.content_home, mapFragment, "2").hide(mapFragment).commit();
        fm.beginTransaction().add(R.id.content_home, overviewFragment, "1").commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            Objects.requireNonNull(fragment.getView()).setVisibility(View.VISIBLE);
            FragmentTransaction tr = fm.beginTransaction();
            tr.hide(curFragment).show(fragment).commit();
            curFragment = fragment;
        }

    }

    @NonNull
    private Fragment rootTabFragment(int tabId) {
        switch (tabId) {
            case 0:
                toolbar.setTitle("Agent Overview");
                return overviewFragment;
            case 1:
                toolbar.setTitle("Agent Location");
                return mapFragment;
            default:
                throw new IllegalArgumentException();
        }
    }


    public void gotoMapFragment(View view){
        replaceFragment(rootTabFragment(1));

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (curFragment == mapFragment) {
            replaceFragment(rootTabFragment(0));
        } else {
            super.onBackPressed();
        }
    }

}
