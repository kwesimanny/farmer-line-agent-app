package co.farmerline.farmerlineagent.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.farmerline.farmerlineagent.Adapters.InputSalesAdapter;
import co.farmerline.farmerlineagent.Pojo.Agent;
import co.farmerline.farmerlineagent.Pojo.Product;
import co.farmerline.farmerlineagent.Pojo.Sale;
import co.farmerline.farmerlineagent.R;
import co.farmerline.farmerlineagent.Realm.RealmManager;
import io.realm.RealmResults;

public class OverviewFragment extends Fragment {

    @BindView(R.id.sales_product_list)
    RecyclerView salesProductList;

    @BindView(R.id.total_product_sales)
    TextView totalProductSales;

    @BindView(R.id.total_farmers_sold_to)
    TextView totalFarmersSoldTo;

    @BindView(R.id.farmers_quota)
    TextView farmersQuota;

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

    }

    private void init() {
        initTextViews();
        initRecyclerViews();
    }

    private void initTextViews() {

        Agent agent = RealmManager.agentRepository().getAgent();
        if (agent != null) {
            totalFarmersSoldTo.setText(agent.getFarmersSoldTo());
            farmersQuota.setText(agent.getFarmersQuota());
        }

        double totalSalesDone = 0;
        RealmResults<Sale> sales = RealmManager.salesRepository().loadAll();
        for (int i = 0; i < sales.size(); i++) {
            totalSalesDone = totalSalesDone + Objects.requireNonNull(sales.get(i)).getCostOfSale();
        }
        totalProductSales.setText("GHS " + String.valueOf(totalSalesDone));
    }


    private void initRecyclerViews() {
        RealmResults<Product> products = RealmManager.productRepository().loadAll();
        ArrayList<Product> productArrayList = new ArrayList<>(products);
        InputSalesAdapter inputSalesAdapter = new InputSalesAdapter(productArrayList);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        salesProductList.setLayoutManager(llm);
        salesProductList.setAdapter(inputSalesAdapter);
    }

 
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
