package co.farmerline.farmerlineagent.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import co.farmerline.farmerlineagent.Pojo.Product;
import co.farmerline.farmerlineagent.R;

public class InputSalesAdapter extends RecyclerView.Adapter<InputSalesAdapter.ViewHolder> {


    private ArrayList<Product> products;

    public InputSalesAdapter(ArrayList<Product> products) {
        this.products = products;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(viewGroup.getContext(), R.layout.item_total_product_sales, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Product product = products.get(i);
        viewHolder.productName.setText(product.getProductName());

        if (product.getQuantitySold() != 0 && product.getQuantityAvailable() != 0) {
            double percentage = (product.getQuantitySold() / product.getQuantityAvailable()) * 100;
            viewHolder.percentageProductSales.setText(String.valueOf(percentage) + "%");
        } else {
            viewHolder.percentageProductSales.setText("0%");
        }

        viewHolder.totalproductSales.setText("Ghs " + product.getTotalSoldPrice());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView productName;
        TextView percentageProductSales;
        TextView totalproductSales;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.product_name);
            percentageProductSales = itemView.findViewById(R.id.percentage_product_sales);
            totalproductSales = itemView.findViewById(R.id.total_product_sales);
        }
    }
}
